# Mytwitter

## Environment Variables

- CONSUMER_KEY  
  Twitter consumer key
- CONSUMER_SECRET  
  Twitter consumer secret
- ACCESS_TOKEN  
  Access token
- ACCESS_TOKEN_SECRET  
  Access token secret
